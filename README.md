yarn start
Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

yarn install then yarn start will easily start the project!!!

yarn build
Builds the app for production to the build folder.

yarn eject
no need for this!