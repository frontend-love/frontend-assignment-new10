import { createGlobalStyle } from 'styled-components'
import futuramedium from './fonts/futuramedium.ttf'
import futuralight from './fonts/futualight.ttf'
import futurabold from './fonts/futurabold.ttf'
import futurabook from './fonts/futurabook.ttf'

const GlobalStyles = createGlobalStyle`
    @font-face {
    font-family: 'Futura Medium';
    src:  url(${futuramedium}) format('truetype');   
}     
    @font-face {
    font-family: 'Futura Light';
    src:  url(${futuralight}) format('truetype');   
} 

   @font-face {
    font-family: 'Futura Bold';
    src:  url(${futurabold}) format('truetype');   
} 

  @font-face {
    font-family: 'Futura Book';
    src:  url(${futurabook}) format('truetype');   
} 
    
    
`

export default GlobalStyles
