import { combineReducers } from 'redux'
import { selectedOptionReducer } from './SelectedOption/selectedOptionReducer'

export default combineReducers({
  selectedOption: selectedOptionReducer,
})
