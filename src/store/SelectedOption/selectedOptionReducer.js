import {
  SELECT_OPTION,
  SELECT_AMOUNT,
  SELECT_DURATION,
} from './selectedOptionAction'

let initialState = {
  product: '',
  legalForm: '',
  amount: null,
  duration: null,
}

export const selectedOptionReducer = (state = initialState, action) => {
  if (action.type === SELECT_OPTION) {
    if (action.value === 'BV' || action.value === 'Eenmanszak') {
      return { ...state, legalForm: action.value }
    } else {
      return { ...state, product: action.value }
    }
  }

  if (action.type === SELECT_AMOUNT) {
    state.amount = action.value
    return { ...state }
  }

  if (action.type === SELECT_DURATION) {
    state.duration = action.value
    return { ...state }
  }
  return state
}
