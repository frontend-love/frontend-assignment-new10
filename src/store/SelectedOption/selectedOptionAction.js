export const SELECT_OPTION = 'SELECT OPTION'
export const SELECT_AMOUNT = 'SELECT AMOUNT'
export const SELECT_DURATION = 'SELECT DURATION'
export const selectOption = (value) => {
  return {
    type: SELECT_OPTION,
    value,
  }
}

export const selectAmount = (value) => {
  return {
    type: SELECT_AMOUNT,
    value,
  }
}

export const selectDuration = (value) => {
  return {
    type: SELECT_DURATION,
    value,
  }
}
