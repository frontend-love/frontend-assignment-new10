import React from 'react'

import { InputArea, InputWrapper, InputSpan } from './Input.sc'

class Input extends React.Component {
  onChange = (e) => {
    if (e.target.validity.valid) this.props.onChange(e.target.value)
  }
  onFocus = (e) => {
    this.props.onFocus(e)
  }

  render() {
    return (
      <InputWrapper>
        {this.props.isAmount && <InputSpan>€</InputSpan>}
        <InputArea
          placeholder={this.props.placeholder}
          type={this.props.type}
          pattern={this.props.pattern}
          value={this.props.value}
          onChange={this.onChange}
          onFocus={this.onFocus}
          isAllFilled={this.props.isAllFilled}
          isAmount={this.props.isAmount}
        />
      </InputWrapper>
    )
  }
}

export default Input
