import styled from 'styled-components'

export const InputWrapper = styled.div`
  cursor: pointer;
  overflow: hidden;
  font-weight: '500';
  font-family: Helvetica, sans-serif;
  font-size: 18px;
  border: solid 0.1rem #a4a5ed;
  border-radius: 5px;
  margin-bottom: 8px;
  &:hover {
    border-color: black;
  }
`

export const InputArea = styled.input`
    cursor:pointer;
    overflow:hidden;
    line-height: 5rem;
    font-weight: '500';
    font-family: Futura Book;
    font-size: 20px;
    border:none;
    outline: none;
    width: ${({ isAmount }) => (isAmount ? '90%' : '100%')}; 
    padding-left:${({ isAmount }) => (isAmount ? '0.2rem' : '2.5rem')};
   ::placeholder {
     color: ${({ isAllFilled }) => (isAllFilled ? 'grey' : 'red')};  
       
   }
    
  }
  
    @media (max-width: 768px) {
    padding-left:${({ isAmount }) => (isAmount ? '0rem' : '0.5rem')};
    font-size: 17px;
  }
  }  
    `

export const InputSpan = styled.span`
  font-size: '18px';
  padding-left: 1.5rem;
  @media (max-width: 768px) {
    padding-left: 0rem;
  }
`
