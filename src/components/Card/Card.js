import React from 'react'

import { connect } from 'react-redux'
import {
  selectOption,
  selectAmount,
  selectDuration,
} from '../../store/SelectedOption/selectedOptionAction'

import Dropdown from '../DropdownMenu/Dropdown/Dropdown'
import Button from '../Button/Button'
import Input from '../Input/Input'

import {
  CardWrapper,
  CardBody,
  CardItemsWrapper,
  CardTitle,
  InformationCard,
  CardDropdown,
  WarningDiv,
} from './Card.sc'

function calculateMaxDuration(selectedOption) {
  let maxDuration = 60

  if (
    selectedOption.product === 'Marketing' &&
    selectedOption.legalForm !== ''
  ) {
    maxDuration = 36
  }

  return maxDuration
}

function calculateMaxAmount(selectedOption) {
  let maxAmount = 250000

  if (
    selectedOption.product === 'Equipment' &&
    selectedOption.legalForm === 'BV'
  ) {
    maxAmount = 500000
  }

  return maxAmount
}

function checkOptionsSelected(selectedOption) {
  if (selectedOption.product === '' && selectedOption.legalForm === '') {
    return 'Please select a Funding Purpose and Desired New Term first'
  }
  if (selectedOption.product === '') {
    return 'Please select a Funding Purpose first'
  }

  if (selectedOption.legalForm === '') {
    return 'Please select a Desired New Term first'
  }

  return ''
}

class Card extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      products: ['Marketing', 'Equipment'],
      legalForms: ['BV', 'Eenmanszak'],
      amountInput: '',
      durationInput: null,
      isButtonClicked: false,
      maxAmount: null,
      maxDuration: 60,
      isOptionCompleted: true,
      windowSize: null,
      allFilled: true,
      calculatedInterestRate: 0,
      isDropdownOptionsSelected: false,
    }
  }

  componentDidMount() {
    this.setState({ windowSize: window.innerWidth })
  }

  handleInputChange = (value) => {
    this.setState({ amountInput: value })
    this.props.getAmount(value)
  }

  handleDurationInputChange = (value) => {
    this.setState({ durationInput: value })
    this.props.getDuration(value)
  }

  handleOnClickOption = (value) => {
    this.props.selectOption(value)
  }

  optionSelectionCheck = () => {
    let optionsCheck = checkOptionsSelected(this.props.selectedOption)
    if (optionsCheck !== '') {
      let style = {
        fontFamily: 'Futura Book',
        fontSize: '19px',
        color: 'red',
      }
      return <div style={style}>{optionsCheck}</div>
    }
  }

  showInterestRate = () => {
    if (
      this.props.selectedOption.product === '' ||
      this.props.selectedOption.legalForm === '' ||
      this.props.selectedOption.amount === null ||
      this.props.selectedOption.product === null
    ) {
      this.setState({ isOptionCompleted: false })
    } else {
      this.setState({ isButtonClicked: true })
      this.calculateInterestRate()
    }
  }

  calculateInterestRate = () => {
    let exactRate = 0
    let selection = this.props.selectedOption
    let d = selection.duration
    let maxDuration = calculateMaxDuration(selection)

    if (selection.amount >= 0 || selection.amount <= 50000) {
      exactRate = (6 + 2 / (d * maxDuration)).toFixed(2)
    } else if (selection.amount > 50000 || selection.amount <= 150000) {
      exactRate = (5 + 2 / (d * maxDuration)).toFixed(2)
    } else if (selection.amount > 150000 || selection.amount <= 500000) {
      exactRate = (4 + 2 / (d * maxDuration)).toFixed(2)
    }
    this.setState({ calculatedInterestRate: exactRate })
    return exactRate
  }

  onFocus = (e) => {
    console.log(e.target.placeholder)
  }

  render() {
    const style = {
      width: '100%',
    }

    const spanStyle = {
      fontWeight: 'bold',
      fontFamily: 'Futura Bold',
    }

    const {
      products,
      legalForms,
      calculatedInterestRate,
      isOptionCompleted,
      allFilled,
    } = this.state
    const { selectedOption } = this.props
    let durationInputProps = {
      placeHolder: `Please enter a duration in months up to ${calculateMaxDuration(
        selectedOption
      )} months`,
    }

    let amountInputProps = {
      placeHolder: `Please enter an amount up to ${calculateMaxAmount(
        selectedOption
      )}`,
    }

    let productDropdownProps = {
      buttonTitle:
        selectedOption.product === ''
          ? 'What do you want to finance?'
          : selectedOption.product,
    }

    let legalFormDropdownProps = {
      buttonTitle:
        selectedOption.legalForm === ''
          ? 'Please choose a term'
          : selectedOption.legalForm,
    }

    return (
      <CardWrapper isButtonClicked={this.state.isButtonClicked}>
        {!this.state.isButtonClicked || this.state.windowSize > 768 ? (
          <CardBody>
            <CardDropdown isButtonClicked={this.state.isButtonClicked}>
              <CardItemsWrapper>
                <CardTitle>Funding Purpose</CardTitle>
                <div style={style}>
                  <Dropdown
                    options={products}
                    {...productDropdownProps}
                    onClickOption={this.handleOnClickOption}
                  />
                  {!isOptionCompleted && (
                    <WarningDiv>Please choose an option</WarningDiv>
                  )}
                </div>
              </CardItemsWrapper>
              <CardItemsWrapper>
                <CardTitle>Desired New Term</CardTitle>
                <div style={style}>
                  <Dropdown
                    options={legalForms}
                    {...legalFormDropdownProps}
                    onClickOption={this.handleOnClickOption}
                  />
                  {!isOptionCompleted && (
                    <WarningDiv>Please choose an option.</WarningDiv>
                  )}
                </div>
              </CardItemsWrapper>
              <CardItemsWrapper>
                <CardTitle>Desired Amount</CardTitle>
                <div style={style}>
                  <Input
                    isAllFilled={allFilled}
                    type="text"
                    pattern="[0-9]*"
                    placeholder={amountInputProps.placeHolder}
                    onChange={this.handleInputChange}
                    onFocus={this.onFocus}
                    value={this.state.amountInput || ''}
                    isAmount={true}
                  />
                  {this.optionSelectionCheck()}
                  {!isOptionCompleted && (
                    <WarningDiv>Please enter a valid number.</WarningDiv>
                  )}
                </div>
              </CardItemsWrapper>
              <CardItemsWrapper>
                <CardTitle>Duration</CardTitle>
                <div style={style}>
                  <Input
                    isAllFilled={allFilled}
                    type="text"
                    pattern="[0-9]*"
                    placeholder={durationInputProps.placeHolder}
                    onChange={this.handleDurationInputChange}
                    value={this.state.durationInput || ''}
                    onFocus={this.onFocus}
                    isAmount={false}
                  />
                  {this.optionSelectionCheck()}
                  {!isOptionCompleted && (
                    <WarningDiv>Please enter a valid number.</WarningDiv>
                  )}
                </div>
              </CardItemsWrapper>
              <CardItemsWrapper>
                <Button onClick={this.showInterestRate}>
                  CHECK ELLIGIBILITY
                </Button>
              </CardItemsWrapper>
            </CardDropdown>
            <InformationCard isButtonClicked={this.state.isButtonClicked}>
              <p>
                Welcome! You are looking for financing of{' '}
                <span style={spanStyle}>{selectedOption.amount}</span> to pay
                for <span style={spanStyle}>{selectedOption.product}</span>.{' '}
                <br />
                Our interest rate calculated for this selection is{' '}
                <span style={spanStyle}>{calculatedInterestRate}%</span>. <br />
                Are you curious about what interest rate we can offer you?{' '}
                <br /> We need some more information to be able to make you a
                tailor-made offer without obligation. <br />
                After your registration you know in 3 steps how much you can
                borrow and at what interest.
                <br />
                You can choose whether to convert the offer into a request.
              </p>
            </InformationCard>
          </CardBody>
        ) : (
          <InformationCard isButtonClicked={this.state.isButtonClicked}>
            <p>
              Welcome! You are looking for financing of{' '}
              <span style={spanStyle}>{selectedOption.amount}</span> to pay for{' '}
              <span style={spanStyle}>{selectedOption.product}</span>. <br />
              Our interest rate calculated for this selection is{' '}
              <span style={spanStyle}>{calculatedInterestRate}%</span>. <br />
              Are you curious about what interest rate we can offer you? <br />{' '}
              We need some more information to be able to make you a tailor-made
              offer without obligation. <br />
              After your registration you know in 3 steps how much you can
              borrow and at what interest.
              <br />
              You can choose whether to convert the offer into a request.
            </p>
          </InformationCard>
        )}
      </CardWrapper>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    selectOption: (value) => dispatch(selectOption(value)),
    getAmount: (value) => dispatch(selectAmount(value)),
    getDuration: (value) => dispatch(selectDuration(value)),
  }
}

const mapStateToProps = (state) => {
  return {
    selectedOption: state.selectedOption,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Card)
