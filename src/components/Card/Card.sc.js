import styled from 'styled-components'

export const CardWrapper = styled.div`
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

export const CardDropdown = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  div:nth-child(5) {
    margin-left: auto;
  }
  ${({ isButtonClicked }) =>
    isButtonClicked &&
    `           
     width:50%;
     div:nth-child(5){
 }
    `}
  @media (max-width: 768px) {
    width: 100%;
    div:nth-child(5) {
      margin-top: 34px;
      margin-left: 0;
    }
  }
  transition: width 3s;
`
export const CardBody = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`

export const CardItemsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`
export const CardTitle = styled.div`
  width: 19%;
  text-align: center;
  height: 6rem;
  padding-top: 38px;
  font-size: 25px;
  font-family: Futura Book;
  font-weight: bold;
  text-align: left;
  padding-left: 20px;
  @media (max-width: 768px) {
    width: 100%;
    height: 3rem;
    padding-top: 10px;
    padding-left: 6px;
  }
`

export const InformationCard = styled.div`
  width: 50%;
  visibility: hidden;
  opacity: 0;
  margin: 15px;
  transition: visibility 3s 3s, opacity 3s ease-in;
  p {
    font-size: 20px;
    font-family: 'Futura Book';
    text-align: center;
    line-height: 55px;
    padding-top: 85px;
    @media (max-width: 768px) {
      padding-top: 0px;
    }
  }
  ${({ isButtonClicked }) =>
    isButtonClicked &&
    ` 
    visibility: visible;
    opacity: 1;
    `}
  @media (max-width: 768px) {
    width: 100%;
    padding: 0;
    margin: 0;
    padding-top: 10px;
  }
`
export const WarningDiv = styled.div`
  font-size: 19px;
  font-family: Futura Book;
  color: red;
  @media (max-width: 768px) {
  }
`
