import React from 'react'
import styled from 'styled-components'

import DropdownOption from './DropdownOption/DropdownOption'

import { OptionWrapper } from './DropdownOption/DropdownOption.sc'

export const DropdownMenuWrapper = styled.div`
  overflow: auto;
  position: relative;
  background-color: #f5f5f5;
  ${OptionWrapper}:last-child {
    ${({ isOpen }) =>
      isOpen &&
      ` 
    border-bottom: solid 0.1rem #a4a5ed; 
    &:hover {
    border-color: black;     
  }
    `}
  }
  ${OptionWrapper}:first-child {
    border-top: none;
    &:hover {
    }
  }
`

const DropdownMenu = ({ isOpen, onClickOption, options, selectedValue }) => {
  return (
    <DropdownMenuWrapper isOpen={isOpen}>
      {options.map((option) => {
        return (
          <DropdownOption
            isSelected={option.value}
            key={option.value}
            isVisible={isOpen}
            label={option.label}
            onClick={onClickOption}
            value={option}
          />
        )
      })}
    </DropdownMenuWrapper>
  )
}

export default DropdownMenu
