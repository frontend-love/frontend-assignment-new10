import styled from 'styled-components'

export const IconWrapper = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  padding: 0;
  width: 1.6rem;
  height: 1.6rem;
  vertical-align: top;
  line-height: 1;
  color: inherit;
  pointer-events: none;

  svg {
    fill: currentColor;
    width: 100%;
    max-width: 100%;
    height: 100%;
    max-height: 100%;
  }
`
export const DropdownButtonWrapper = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  justify-content: center;
  height: 5rem;
  text-align: left;
  border: solid 0.1rem #a4a5ed;
  border-radius: 5px;
  padding: 0 2.5rem;
  font-family: Futura Book;
  font-size:20px;


  &:hover {
    cursor: pointer;
    border-color:black;
    background-color: #f5f5f5
  }
  
    ${IconWrapper} {
    position: absolute;
    right: 3.5rem;
    width: 2.4rem;
    height: 2.4rem;
    ${({ isPushed }) => isPushed && `transform: rotate(180deg);`};

    & svg {
      fill: black;
    }
    right:1.5rem;
  }
  
  @media (max-width: 768px) {
    padding: 0 0.5rem;
    font-size:18px;
  }
  }
  `
