import styled from 'styled-components'

export const DropdownWrapper = styled.div`
  display: block;
  position: relative;
  min-width: 22.8rem;
  height: 6rem;
  text-align: left;
  z-index: ${({ isOpen }) => (isOpen ? '999' : '50')};
  background-color: white;
`

export const DropdownBody = styled.div`
  display: flex;
  width: 100%;
  min-height: 100%;
  z-index: ${({ isOpen }) => (isOpen ? '1000000' : 'auto')};
  flex-direction: column;
  position: absolute;
`
