import React from 'react'

import { DropdownBody, DropdownWrapper } from './Dropdown.sc'

import DropdownMenu from '../DropdownMenu'
import DropdownButton from '../DropdownButton/DropdownButton'

class Dropdown extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
    this.ref = React.createRef()
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  handleClickOutside = (event) => {
    if (
      this.ref &&
      this.ref.current &&
      !this.ref.current.contains(event.target)
    ) {
      this.setState({
        isOpen: false,
      })
    }
  }

  handleDropDownClick = (e) => {
    e.preventDefault()
    e.stopPropagation()
    this.setState((oldState) => ({ isOpen: !oldState.isOpen }))
  }

  handleOnClickOption = (value) => {
    this.props.onClickOption(value)
  }

  render() {
    return (
      <DropdownWrapper
        ref={this.ref}
        isOpen={this.state.isOpen}
        onClick={this.handleDropDownClick}
      >
        <DropdownBody isOpen={this.state.isOpen}>
          <DropdownButton
            isOpen={this.state.isOpen}
            title={this.props.buttonTitle}
          />
          <DropdownMenu
            options={this.props.options}
            isOpen={this.state.isOpen}
            title={this.props.buttonTitle}
            onClickOption={this.handleOnClickOption}
          />
        </DropdownBody>
      </DropdownWrapper>
    )
  }
}

export default Dropdown
