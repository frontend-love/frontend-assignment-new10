import React from 'react'

import './DropdownOption.sc.js'
import { OptionWrapper } from './DropdownOption.sc'

const DropDownOption = ({ label, onClick, isSelected, isVisible, value }) => {
  return (
    <OptionWrapper
      isSelected={isSelected}
      isVisible={isVisible}
      onClick={() => {
        onClick(value)
      }}
    >
      {label || value}
    </OptionWrapper>
  )
}

export default DropDownOption
