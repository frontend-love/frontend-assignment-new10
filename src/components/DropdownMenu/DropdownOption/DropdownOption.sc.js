import styled from 'styled-components'

export const OptionWrapper = styled.div`
  cursor: pointer;
  padding: 0 2.5rem;
  height: ${({ isVisible }) => (isVisible ? '4rem' : 0)};
  overflow: hidden;
  line-height: 4rem;
  font-weight: 300;
  font-family: Futura Book;
  font-size: 18px;

  ${({ isVisible }) =>
    isVisible &&
    `    
      transition: height 200ms;
      border-top: solid 0.1rem #a4a5ed;
      border-right: solid 0.1rem #a4a5ed;
      border-left: solid 0.1rem #a4a5ed;
      border-radius: 5px;
    `}
  &:hover {
    border-color: black;
    background-color: #dcdcdc;
  }

  @media (max-width: 768px) {
    font-size: 17px;
  }
`
