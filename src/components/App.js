import React from 'react'
import { Provider } from 'react-redux'

import GlobalStyles from '../GlobalStyles'

import { store } from '../store'

import Card from './Card/Card'

class App extends React.Component {
  render() {
    return (
      <div>
        <GlobalStyles />
        <Provider store={store}>
          <Card />
        </Provider>
      </div>
    )
  }
}

export default App
