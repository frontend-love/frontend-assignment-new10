import React from 'react'
import styled from 'styled-components'

export const ButtonWrapper = styled.button`
  cursor: pointer;
  padding: 0 2.5rem;
  line-height: 4rem;
  font-family: Futura Book;
  font-size: 17px;
  background-color: white;
  border-radius: 5px;
  outline: none;
  &:hover {
    background-color: #d0d0d0;
  }
`

const Button = ({ onClick, children }) => {
  return <ButtonWrapper onClick={onClick}>{children}</ButtonWrapper>
}

export default Button
